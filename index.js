/* 
1. Write a JavaScript function that returns a passed string with letters in alphabetical order.

    Example string: 'webmaster'
    Expected output: 'abeemrstw'
 */

let word = prompt("1.) Please input a string: ");

while (word === "") {
  word = prompt("1.) Input cannot be empty. Please input a string: ");
}

console.log("You've entered the string: ");
console.log(word);

const wordAlphabetical = (word) => word.split("").sort().join("");

let arrangedWord = wordAlphabetical(word);
console.log("The string arranged alphabetically is: ");
console.log(arrangedWord);

/*
2. Write a JavaScript function that accepts a string as a parameter and counts the number of vowels within the string.
    
    Example string: 'The quick brown fox'
    Expected output: 5
*/
console.log("");

let word1 = prompt("2.) Please input a string: ");

while (word1 === "") {
  word1 = prompt("2.) Input cannot be empty. Please input a string: ");
}

function vowelNumber(word1) {
  const vowelCount = word1.match(/[aeiou]/gi).length;

  return vowelCount;
}

let vowels = vowelNumber(word1);
console.log("The number of vowels in your string is: ");
console.log(vowels);

/* 
3. Write a JavaScript function that inserts new elements at the beginning of the array and sorts them in alphabetical order. Add the rest of the member countries of South East Asia
 */

console.log("");

let countries = [];

let sea = [
  "Singapore",
  "Philippines",
  "Thailand",
  "Indonesia",
  "Vietnam",
  "Cambodia",
  "Malaysia",
  "Myanmar",
  "Laos",
  "Brunei",
  "Timor-Leste",
];

function addCountries() {
  while (countries.length !== sea.length) {
    let addCountry = prompt(
      "3.) Input a Southeast Asian Country: (" +
        countries.length +
        "/" +
        sea.length +
        ")"
    );
    countries.unshift(addCountry);
  }

  let countriesString = JSON.stringify(countries.sort());
  let seaString = JSON.stringify(sea.sort());
  if (countriesString === seaString) {
    return countriesString;
  } else {
    console.log("You've entered the wrong countries: ");
    console.log(countries);
  }
}

let countriesArranged = addCountries();
console.log(countriesArranged);

/* 
4. Using object literals, create a person object with the following properties:

    firstName, lastName, age, gender, nationality. Print object in the console.
*/
console.log("");

let person = {
  firstName: "Levin",
  lastName: "Alvarez",
  age: 23,
  gender: "Male",
  nationality: "Filipino",
};

console.log("Result from creating object using Object Literals: ");
console.log(person);

/* 
5. Using construcor function, create 3 objects with the same structure. These 3 objects must have at least 5 key-value pairs, and 1 of the property must be a method that allows the object to print a message in the console.
*/
console.log("");

function Person(firstName, lastName, age, gender, nationality) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.age = age;
  this.gender = gender;
  this.nationality = nationality;
  this.introduce = function () {
    console.log(
      `Hi! My name is ${firstName} ${lastName} and I am ${age} years old. I specify myself as a ${gender} and I am also a proud ${nationality}.`
    );
  };
}
let person1 = new Person("John", "Doe", 12, "Male", "American");
person1.introduce();

let person2 = new Person("Usain", "Bolt", 36, "Male", "Nigerian");
person2.introduce();

let person3 = new Person("Maggie", "Cheung", 58, "Female", "Chinese");
person3.introduce();
